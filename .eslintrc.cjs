module.exports = {
  root: true,
  env: { browser: true, es2020: true },
  extends: [
    'airbnb',
    'prettier',
    'plugin:@typescript-eslint/recommended',
    'plugin:react/recommended',
    'plugin:jsx-a11y/recommended',
  ],
  ignorePatterns: ['dist', '.eslintrc.cjs'],
  parser: '@typescript-eslint/parser',
  plugins: ['react-refresh'],
  overrides: [
    {
      files: ['*.ts', '*.tsx'],
      rules: {
        indent: ['error', 2, { SwitchCase: 1 }],
        'import/prefer-default-export': ['off'],
        'react/react-in-jsx-scope': 'off',
        'no-restricted-exports': 'off',
        'import/no-unresolved': 'off',
        'import/extensions': 'off',
        'react/jsx-filename-extension': 'off',
        'react-hooks/exhaustive-deps': 'off',
        'no-console': 'off',
        '@typescript-eslint/no-explicit-any': 'warn',
        '@typescript-eslint/naming-convention': [
          'error',
          { selector: 'interface', prefix: ['I'], format: ['PascalCase'] },
          { selector: 'property', format: ['camelCase'] },
          {
            selector: 'objectLiteralProperty',
            format: ['camelCase', 'snake_case'],
          },
        ],
        'react-refresh/only-export-components': ['warn', { allowConstantExport: true }],
      },
    },
  ],
};
