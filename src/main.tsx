import React from 'react';
import ReactDOM from 'react-dom/client';
import App from 'pages/app';

import 'shared/style/global-style.css';
import 'shared/lib/normalize.css';

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
);
