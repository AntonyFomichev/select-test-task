import { useRef, useState } from 'react';
import useClickOutside from 'shared/utils/useClickOutside.ts';
import useUsers from 'features/select-users/slices/users';
import UserList from './components/user-list';
import { SelectedUserType } from './types.ts';

import './select.css';

export function SelectUsers() {
  const { users, page, selectRef, isOpen, loading, error, setIsOpen } = useUsers(50);
  const [selectedUser, setSelectedUser] = useState<SelectedUserType | null>(null);
  const containerRef = useRef<HTMLDivElement>(null);

  // for close expanded list after click away
  useClickOutside(containerRef, () => setIsOpen(false));

  const handleToggle = () => {
    setIsOpen(!isOpen);
  };

  const handleSelect = ({ id, value }: SelectedUserType) => {
    setSelectedUser({ id, value });
    setIsOpen(false);
  };

  return (
    <div className="custom-scroll" ref={containerRef}>
      <label htmlFor="user-select" className="custom-scroll__label">
        Users
      </label>

      <div
        id="user-select"
        className={`custom-scroll__select${isOpen ? ' custom-scroll__select--selected' : ''}`}
        onClick={handleToggle}
        aria-hidden="true"
      >
        <span className="custom-scroll__value">{(selectedUser && selectedUser.value) || 'Select user'}</span>

        <img src="/expand_arrow.svg" alt="expand-arrow" className="custom-scroll__arrow" />
      </div>

      {error && <div className="custom-scroll__error">{error}</div>}

      {isOpen && !error && (
        <UserList
          users={users}
          selectedUser={selectedUser}
          handleSelect={handleSelect}
          loading={loading}
          selectRef={selectRef}
          isFirstPage={page === 1}
        />
      )}
    </div>
  );
}
