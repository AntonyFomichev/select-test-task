import { useEffect, useRef, useState } from 'react';
import fetchApi from 'shared/utils/apiClient.ts';
import debounce from 'shared/utils/debounce.ts';
import { UsersDataType, IUsersResponse } from './types';

const THRESHOLD = 100;

// request users, add scroll logic
const useUsers = (limit: number) => {
  const [isOpen, setIsOpen] = useState(false);
  const [users, setUsers] = useState<UsersDataType[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);
  const [totalUsers, setTotalUsers] = useState<number>(0);
  const [page, setPage] = useState(1);
  const selectRef = useRef<HTMLUListElement | null>(null);

  const fetchUsers = async (pageNumber: number) => {
    setLoading(true);
    setError(null);

    try {
      const response = await fetchApi(`/users?page=${pageNumber}&limit=${limit}`);
      const select = selectRef.current;

      const { data, meta } = (await response) as IUsersResponse;

      if (pageNumber >= page) {
        if (pageNumber !== 1) {
          // if direction scroll down, get current users data and one last user to make scroll more transparent
          setUsers((prevState) => [prevState[prevState.length - 1], ...data]);
        } else {
          setUsers(data); // if page 1, just get user data
        }
        select?.scrollTo(0, 36); // scroll top by element container
      } else {
        // if direction scroll up, get current user data
        setUsers(data);
        select?.scrollTo(0, select.scrollHeight - (select.clientHeight + THRESHOLD)); // scroll bottom + threshold
      }

      setTotalUsers(meta.total);
    } catch (e: any) {
      setError(e.message);
    }

    setLoading(false);
  };

  // set delay for scrolling to not trigger it all the time on scroll
  const handleScroll = debounce(() => {
    const select = selectRef.current;

    if (!loading && select) {
      // if direction scroll up
      if (select.scrollTop + select.clientHeight === select.scrollHeight && users.length >= limit) {
        fetchUsers(page + 1);
        setPage((prevState) => prevState + 1);
      }
      // if direction scroll down
      else if (select.scrollTop === 0 && page - 1 !== 0) {
        fetchUsers(page - 1);
        setPage((prevState) => prevState - 1);
      }
    }
  }, 20);

  useEffect(() => {
    if (isOpen) {
      // fetch users after select expanded
      fetchUsers(page);
    } else {
      // if select close, get initial values of users and page
      setUsers([]);
      setPage(1);
    }
  }, [isOpen]);

  // set event listeners on scroll
  useEffect(() => {
    if (totalUsers !== users.length && !loading) {
      selectRef.current?.addEventListener('scroll', handleScroll);
    } else {
      selectRef.current?.removeEventListener('scroll', handleScroll);
    }

    return () => {
      selectRef.current?.removeEventListener('scroll', handleScroll);
    };
  }, [users, totalUsers, loading, page]);

  return {
    users,
    page,
    selectRef,
    isOpen,
    loading,
    error,
    setIsOpen,
  };
};

export default useUsers;
