type UsersDataType = {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    job?: string;
}

type UsersMetaType = {
    from: number;
    to: number;
    total: number;
}

interface IUsersResponse {
    data: Array<UsersDataType>;
    meta: UsersMetaType
}

export type { UsersDataType, UsersMetaType, IUsersResponse };
