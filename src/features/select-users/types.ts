export type SelectedUserType = {
  id: number;
  value: string;
};
