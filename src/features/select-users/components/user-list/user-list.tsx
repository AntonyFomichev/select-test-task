import { IUserList } from './types.ts';
import './user-list.css';

export function UserList({ users, selectedUser, handleSelect, loading, selectRef, isFirstPage }: IUserList) {
  return (
    <ul className="custom-scroll__list" ref={selectRef}>
      {!isFirstPage && !loading && (
        <li key="item_start" className="custom-scroll__item custom-scroll__item--disabled" />
      )}

      {users.map((user) => (
        <li
          key={user.id}
          onClick={() =>
            handleSelect({
              id: user.id,
              value: `${user.lastName} ${user.firstName}, ${user.job}`,
            })
          }
          className={`custom-scroll__item${selectedUser && user.id === selectedUser.id ? ' custom-scroll__item--selected' : ''}`}
          aria-hidden="true"
        >
          <div className="custom-scroll__icon">{user.lastName.substring(0, 1)}</div>
          {`${user.lastName} ${user.firstName}, ${user.job}`}
        </li>
      ))}

      {!loading && <li key="item_end" className="custom-scroll__item custom-scroll__item--disabled" />}

      {loading && (
        <li key="loading" className="custom-scroll__item custom-scroll__item--disabled">
          Loading...
        </li>
      )}
    </ul>
  );
}
