import { RefObject } from 'react';
import { UsersDataType } from 'features/select-users/slices/users/types.ts';
import { SelectedUserType } from '../../types.ts';

export interface IUserList {
  users: UsersDataType[];
  isFirstPage: boolean;
  selectedUser: SelectedUserType | null;
  handleSelect: ({ id, value }: SelectedUserType) => void;
  loading: boolean;
  selectRef: RefObject<HTMLUListElement>;
}
