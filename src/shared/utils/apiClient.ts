import { BASE_HTTP_URL } from 'shared/constants/env.ts';
import snakeToCamel from './snakeToCamel.ts';

// custom wrapper for fetch
const fetchApi = (url: string, init?: RequestInit) =>
  fetch(`${BASE_HTTP_URL}${url}`, {
    method: init?.method || 'GET',
    ...init,
  })
    .then((response) => {
      if (!response.ok) {
        throw new Error('Failed to fetch');
      }

      return response.json();
    })
    .then((data) => snakeToCamel(data));

export default fetchApi;
