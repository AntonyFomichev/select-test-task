const snakeToCamel = (obj: any): any => {
  if (obj === null || typeof obj !== 'object') {
    return obj;
  }

  if (Array.isArray(obj)) {
    return obj.map((item) => snakeToCamel(item));
  }

  return Object.keys(obj).reduce((acc: any, key: string) => {
    const camelKey = key.replace(/_([a-z])/g, (_, char) => char.toUpperCase());
    let value = obj[key];
    if (typeof value === 'object' && value !== null) {
      value = snakeToCamel(value);
    }
    acc[camelKey] = value;
    return acc;
  }, {});
};

export default snakeToCamel;
